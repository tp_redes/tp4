# Streaming Server

import socket
import time
import sys
from random import randint

HOST = 'localhost'
PORT = 50007

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)

while True:
    conn, addr = s.accept()
    f = open(sys.argv[1],"rb")
    l = f.read(1024)
    i = 0
    print 'Client connection accepted ', addr
    while (l):
        try:
            print 'Server sent packet: ', i
            i = i + 1
            conn.send(l)
            l = f.read(1024)    
        except (socket.error):
            print 'Client connection closed', addr
            break
    f.close()
    print 'All packets were sent'
    conn.close()
conn.close()