#  Streaming Client

import socket
import time

HOST = 'localhost'
PORT = 50007

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
i = 0
while True:
    data = s.recv(1024)
    time.sleep(0.005)
    print 'Client receive packet ', i
    i = i + 1
    if not data:
        print 'Successfully get the file'
        break

s.close()
print 'Connection close'